# **Desenvolvedor FullStack Pleno - Desafio com 1 Semana de Prazo**

Este projeto tem como objetivo avaliar a proficiência do profissional no desenvolvimento de uma aplicação básica para arrecadação de doações, utilizando as tecnologias solicitadas. O prazo para entrega é de **1 semana**.

---

### **Pré-Requisitos Backend**

- **Laravel 8+ com PHP 8.0+**
- **Autenticação Básica (JWT ou OAuth 2.0)**
- **Camada de Persistência** (usando Eloquent, com no máximo 2 tabelas principais)
- **Testes Automatizados** (Testes básicos com PHPUnit)

---

### **Pré-Requisitos Frontend**

- **React 18+**
- **TypeScript**
- **UI Framework** (Escolha entre MUI, Tailwind ou Bootstrap)
- **Autenticação Frontend** (Integração simples com JWT ou OAuth)

---

### **Projeto**

#### **Aplicativo de Arrecadação de Doações para Instituições Beneficentes**

##### **História do Usuário**

- **Cadastro e Login de Usuário**: O usuário deve poder criar uma conta e fazer login com JWT.
- **Escolha de Instituição**: O usuário pode visualizar uma lista simples de instituições (máximo de 5 instituições).
- **Doações**: O usuário pode realizar uma doação única (mínimo de R$ 5,00) para uma instituição de sua escolha.
- **Histórico de Doações**: O usuário pode consultar o histórico de suas doações (apenas doações únicas, sem recorrência).
- **Favoritos**: O usuário pode marcar até **1 instituição** como favorita.

##### **Requisitos Técnicos**

- **Backend (Laravel)**:
  - Implementação de **Autenticação JWT** para login de usuários.
  - Criação de tabelas no banco de dados para **Usuários** e **Doações**.
  - API RESTful para gerenciar o processo de doações (endpoints para criar doações e consultar histórico).
  
- **Frontend (React + TypeScript)**:
  - Tela de **Cadastro/Login** com autenticação usando JWT.
  - Tela para **Escolha da Instituição** e realização de **Doações** (com valores fixos).
  - Tela de **Histórico de Doações**.
  - Implementação de **Favoritos** para que o usuário possa salvar uma instituição.

---

### **O que será avaliado**

- **Clean Code**: O código deve ser limpo, modular e legível.
- **Estrutura de Camadas**: Separação entre backend e frontend, com foco na organização do código.
- **Autenticação Simples**: Uso adequado de JWT ou OAuth para autenticação.
- **Interface do Usuário**: Design simples e funcional.
- **Testes Automatizados**: Testes básicos para garantir o funcionamento correto das funcionalidades.

---

### **Diferenciais**

- **Docker**: Usar Docker para criar um ambiente de desenvolvimento.
- **Deploy**: Instruções simples de deploy para uma plataforma de sua escolha (Heroku, DigitalOcean, etc).
- **Notificações Simples**: Implementação básica de envio de e-mail ou alertas simples sobre doações realizadas (opcional).

---

### **Como Entregar**

- O projeto deve ser entregue até **7 dias após o início** no repositório Git.
- Instruções claras de como rodar o projeto localmente ou em produção.
- O código deve ser bem organizado, com um arquivo `README.md` explicando como rodar a aplicação.
- **Critério de Avaliação**: Serão avaliadas a clareza do código, a organização do repositório, e a funcionalidade central da aplicação (doações, login e histórico).

---
